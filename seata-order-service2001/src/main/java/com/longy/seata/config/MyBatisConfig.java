package com.longy.seata.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan({"com.longy.seata.dao"})
public class MyBatisConfig {

}
