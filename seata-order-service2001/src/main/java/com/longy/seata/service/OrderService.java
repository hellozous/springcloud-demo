package com.longy.seata.service;

import com.longy.seata.domain.Order;

public interface OrderService{
    void create(Order order);
}
