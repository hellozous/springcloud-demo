package com.longy.seata.service.impl;

import com.longy.seata.dao.OrderDao;
import com.longy.seata.domain.Order;
import com.longy.seata.service.AccountService;
import com.longy.seata.service.OrderService;
import com.longy.seata.service.StorageService;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
@Slf4j
public class OrderServiceImpl implements OrderService {
    @Resource
    private OrderDao orderDao;

    @Resource
    private StorageService storageService;

    @Resource
    private AccountService accountService;

    /**
     * 创建订单-->调用库存服务扣库存-->调用账户服务扣钱-->修改订单状态-->结束
     * 简单说
     * 下订单-->减库存-->扣钱-->改状态
     * @param order
     */
    @Override
    @GlobalTransactional(name = "fsp-create-order",rollbackFor = Exception.class)
    public void create(Order order) {
        log.info("-----------》开始新建订单........................");
        orderDao.create(order);

        log.info("-----------》订单微服务开始调用减库存................做折扣.");
        Long productId = order.getProductId();
        Integer count = order.getCount();
        storageService.decrease(productId,count);
        log.info("-----------》订单微服务开始调用减库存................做折扣.");

        log.info("-----------》微服务开始调用账户，开始扣钱........................");
        accountService.decrease(order.getUserId(),order.getMoney());
        log.info("-----------》微服务开始调用账户，结束扣钱........................");

        log.info("-----------》修改订单状态，开始........................");
        orderDao.update(order.getUserId(),0);
        log.info("-----------》修改订单状态，结束........................");

        log.info("-----------》订单结束，O(∩_∩)O哈哈~........................");
    }
}
