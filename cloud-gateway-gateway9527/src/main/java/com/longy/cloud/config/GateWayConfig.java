package com.longy.cloud.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GateWayConfig {
    /**
     *  routes:
     *         - id: payment_routh #payment_route    #路由的ID，没有固定规则但要求唯一，建议配合服务名
     *           uri: http://localhost:8001          #匹配后提供服务的路由地址
     * #          uri: lb://cloud-payment-service #匹配后提供服务的路由地址
     *           predicates:
     *             - Path=/Payment/getPaymentById/**         # 断言，路径相匹配的进行路由
     * 配置规则 localhost：9527/guonei   转发到http://news.baidu.com/guonei
     * @param routeLocatorBuilder
     * @return
     */

    @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder routeLocatorBuilder){
        RouteLocatorBuilder.Builder routes = routeLocatorBuilder.routes();

        routes.route("path_route_longy",r -> r.path("/guonei").uri("http://news.baidu.com/guonei")).build();
        routes.route("path_route_longy2",r -> r.path("/guoji").uri("http://news.baidu.com/guoji")).build();

        return routes.build();
    }
}
