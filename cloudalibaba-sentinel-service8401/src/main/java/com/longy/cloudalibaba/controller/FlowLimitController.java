package com.longy.cloudalibaba.controller;

import com.longy.springcloud.domain.CommonResult;
import com.longy.springcloud.domain.Payment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@RestController
@Slf4j
public class FlowLimitController {
//    @Resource
//    private feignClient feignClient;
//
//
//    @GetMapping(value = "/paymentSQL/{id}")
//    public CommonResult<Payment> paymentSQL(@PathVariable("id") Long id){
//        return feignClient.paymentSQL(id);
//    }
    @GetMapping("/testA")
    public String testA() {
//        try {
//            TimeUnit.MILLISECONDS.sleep(800);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        return "------testA";
    }

    @GetMapping("/testB")
    public String testB() {
        log.info(Thread.currentThread().getName()+"\t"+"testB");
        return "------testB";
    }

    @GetMapping("/testD")
    public String testD()
    {
//        try { TimeUnit.SECONDS.sleep(1); } catch (InterruptedException e) { e.printStackTrace(); }

        log.info("testD 测试RT");
        int age = 10/0;
        return "------testD";
    }


}
