package longy.springcloud.service.impl;

import com.longy.springcloud.domain.CommonResult;
import longy.springcloud.service.PaymentFeignService;
import org.springframework.stereotype.Component;

@Component
public class PaymentFallbackServiceImpl implements PaymentFeignService {
    @Override
    public String testFeign() {
        return "服务器忙，请稍后再试.....";
    }

    @Override
    public CommonResult getPaymentById(Long id) {
        return new CommonResult();
    }

    @Override
    public String timeout() {
        return "服务器忙，请稍后再试.....";
    }

    @Override
    public String paymentCircuitBreaker(Integer id) {
        return "服务器忙，请稍后再试.....";
    }
}
