package longy.springcloud.service;

import com.longy.springcloud.domain.CommonResult;
import longy.springcloud.service.impl.PaymentFallbackServiceImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient(value = "CLOUD-PAYMENT-SERVICE",fallback = PaymentFallbackServiceImpl.class)
public interface PaymentFeignService {
    @GetMapping(value = "/testFeign")
    public String testFeign();

    @GetMapping(value = "Payment/getPaymentById/{id}")
    public CommonResult getPaymentById(@PathVariable("id") Long id);

    @GetMapping(value = "/payment/feign/timeout")
    public String timeout();

    @GetMapping(value = "/payment/paymentCircuitBreaker/{id}")
    public String paymentCircuitBreaker(@PathVariable("id") Integer id);
}
