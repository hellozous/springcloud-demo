package longy.springcloud.controller;

import com.longy.springcloud.domain.CommonResult;
import longy.springcloud.service.PaymentFeignService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class OrderController {
    @Resource
    private PaymentFeignService paymentFeignService;

    @GetMapping(value = "/testFeign")
    public String testFeign(){
        return paymentFeignService.testFeign();
    };

    @GetMapping(value = "Payment/getPaymentById/{id}")
    public CommonResult getPaymentById(@PathVariable("id") Long id){
        return paymentFeignService.getPaymentById(id);
    }

    @GetMapping(value = "/payment/feign/timeout")
    public String timeout(){
        return paymentFeignService.timeout();
    }

    @GetMapping(value = "/payment/paymentCircuitBreaker/{id}")
    public String paymentCircuitBreaker(@PathVariable("id") Integer id){
        return paymentFeignService.paymentCircuitBreaker(id);
    }
}
