package com.longy.cloudalibaba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class OrderNacosMain8010 {
    public static void main(String[] args) {
        SpringApplication.run(OrderNacosMain8010.class,args);
    }
}
