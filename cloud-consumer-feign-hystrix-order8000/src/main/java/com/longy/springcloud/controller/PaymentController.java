package com.longy.springcloud.controller;

import com.longy.springcloud.service.PaymentService;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@DefaultProperties(defaultFallback = "Payment_defaultFallbackMethod")
public class PaymentController {
    @Resource
    private PaymentService paymentService;

    @GetMapping("/consumer/Hystrix/payment/ok/{id}")
    public String PaymentInfo_ok(@PathVariable("id") Integer id) {
        String result = paymentService.PaymentInfo_ok(id);
        System.out.println("result:" + result);
        return result;
    }

    @GetMapping("/consumer/Hystrix/payment/timeout/{id}")
//    @HystrixCommand(fallbackMethod = "PaymentInfo_timeoutHandlerMethod",commandProperties = {
//            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value = "2000")})
    @HystrixCommand
    public String PaymentInfo_timeout(@PathVariable("id") Integer id) {
        String result = paymentService.PaymentInfo_timeout(id);
        System.out.println("result:" + result);
        return result;
    }

    public String PaymentInfo_timeoutHandlerMethod(Integer id){
        return "线程池：" + Thread.currentThread().getName()+"  我是80controller  系统繁忙请稍后再试！ ";
    }

    public String Payment_defaultFallbackMethod(){
        return "线程池：" + Thread.currentThread().getName()+"  我是80 defaultFallbackMethod 出错了！！！ ";
    }
}
