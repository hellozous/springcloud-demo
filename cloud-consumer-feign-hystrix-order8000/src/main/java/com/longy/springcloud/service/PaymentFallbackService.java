package com.longy.springcloud.service;

import org.springframework.stereotype.Component;

@Component
public class PaymentFallbackService implements PaymentService {
    @Override
    public String PaymentInfo_ok(Integer id) {
        return "PaymentFallbackService ---PaymentInfo_ok---o(╥﹏╥)o";
    }

    @Override
    public String PaymentInfo_timeout(Integer id) {
        return "PaymentFallbackService ---PaymentInfo_timeout---o(╥﹏╥)o";
    }
}
