package com.longy.cloudalibaba.feignClient;

import com.longy.springcloud.domain.CommonResult;
import com.longy.springcloud.domain.Payment;
import org.springframework.stereotype.Component;

@Component
public class feignClientFallback implements feignClientService{

    @Override
    public CommonResult<Payment> paymentSQL(Long id) {
        return new CommonResult<>(444,"兜底方法",new Payment(id,"null"));
    }
}
