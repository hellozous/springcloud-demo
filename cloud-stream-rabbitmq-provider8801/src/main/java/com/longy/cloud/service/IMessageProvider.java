package com.longy.cloud.service;

public interface IMessageProvider {
    public String send();
}
