package com.longy.springcloud.service.impl;

import com.longy.springcloud.dao.PaymentDao;
import com.longy.springcloud.domain.Payment;
import com.longy.springcloud.service.PaymentService;
import com.longy.springcloud.service.testService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;


@Service
public class PaymentServicesImpl implements PaymentService {
    @Resource
    private PaymentDao paymentDao;

    @Resource
    private testService testService;

    @Override
    public int Creat(Payment payment) {
        return paymentDao.Creat(payment);
    }

    @Override
    public Payment getPaymentById(Long id) {


        Payment paymentById = paymentDao.getPaymentById(id);

        return paymentById;

    }
}
