package com.longy.springcloud.controller;

import com.longy.springcloud.service.PaymentService;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.extern.log4j.Log4j;
import org.apache.juli.logging.Log;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.logging.Logger;

@RestController
public class PaymentController {
    @Resource
    private PaymentService paymentService;

    @GetMapping("Hystrix/payment/ok/{id}")
    public String PaymentInfo_ok(@PathVariable("id") Integer id) {
        String result = paymentService.PaymentInfo_ok(id);
        System.out.println("result:" + result);
        return result;
    }
    @GetMapping("Hystrix/payment/timeout/{id}")
    public String PaymentInfo_timeout(@PathVariable("id") Integer id) {
        String result = paymentService.PaymentInfo_timeout(id);
        System.out.println("result:" + result);
        return result;
    }

    @GetMapping("Hystrix/payment/circuit/{id}")
    public String circuit(@PathVariable("id") Integer id) {
        String result = paymentService.paymentCircuitBreaker(id);
        System.out.println("result:" + result);
        return result;
    }

}
