package com.longy.springcloud.dao;

import com.longy.springcloud.domain.Payment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface PaymentDao {
    public int Creat(Payment payment);

    public Payment getPaymentById(@Param("id") Long id);
}
