package com.longy.springcloud.controller;

import com.longy.springcloud.domain.CommonResult;
import com.longy.springcloud.domain.Payment;
import com.longy.springcloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@RestController
@Slf4j
public class PaymentController {
    @Resource
    private PaymentService paymentService;

    @Value("${server.port}")
    private String serverPort;

    @GetMapping(value = "/testFeign")
    public String testFeign() {

        return "这是8002的testFeign";
    }

    @PostMapping(value = "Payment/Creat")
    public CommonResult Creat(@RequestBody Payment payment) {
        int result = paymentService.Creat(payment);
        log.info("插入结果" + result);
        if (result > 0) {
            return new CommonResult(200, "插入成功,+serverPort:"+serverPort);
        } else {
            return new CommonResult(201, "插入失败");
        }
    }

    @GetMapping(value = "Payment/getPaymentById/{id}")
    public CommonResult getPaymentById(@PathVariable("id") Long id) {
        Payment paymentById = paymentService.getPaymentById(id);
        System.out.println("8002--------------------------------------------");
        log.info("查询结果" + paymentById);
        return new CommonResult(200, "查询成功,serverPort:"+serverPort, paymentById);
    }

    @GetMapping(value = "Payment/lb")
    public String timeout(){

        return serverPort;
    }
}
