package com.longy.springcloud.service.impl;

import com.longy.springcloud.dao.PaymentDao;
import com.longy.springcloud.domain.Payment;
import com.longy.springcloud.service.PaymentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class PaymentServicesImpl implements PaymentService {
    @Resource
    private PaymentDao paymentDao;

    @Override
    public int Creat(Payment payment) {
        return paymentDao.Creat(payment);
    }

    @Override
    public Payment getPaymentById(Long id) {
        return paymentDao.getPaymentById(id);
    }
}
