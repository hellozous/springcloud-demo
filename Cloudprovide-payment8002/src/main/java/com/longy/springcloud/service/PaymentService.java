package com.longy.springcloud.service;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;


public interface PaymentService {
    public int Creat(com.longy.springcloud.domain.Payment payment);

    public com.longy.springcloud.domain.Payment getPaymentById(Long id);
}
